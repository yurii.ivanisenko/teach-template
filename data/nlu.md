## intent:intent_1
- Wann fängt die Ausbildung an
- Wann fängt die Ausbildung an?

## intent:intent_2
- Wie ist die Ausbildung strukturiert
- Wie ist die Ausbildung strukturiert?

## intent:intent_3
- Wie lang geht die Ausbildung
- Wie lang geht die Ausbildung?

## intent:intent_4
- Wie hoch ist die Ausbildungsvergütung
- Wie hoch ist die Ausbildungsvergütung?

## intent:intent_5
- Wie viel Urlaub habe ich
- Wie viel Urlaub habe ich?

## intent:intent_6
- Wie lang ist die Probezeit
- Wie lang ist die Probezeit?

## intent:intent_7
- Kann ich die Ausbildungszeit verkürzen
- Kann ich die Ausbildungszeit verkürzen?

## intent:intent_8
- Gibt es die Möglichkeit neben der Ausbildung zu studieren
- Gibt es die Möglichkeit neben der Ausbildung zu studieren?

## intent:intent_9
- Wie groß sind die Ãœbernahmechancen
- Wie groß sind die Ãœbernahmechancen?

## intent:intent_10
- Welche Angebote/Projekte gibt es neben der Ausbildung
- Welche Angebote/Projekte gibt es neben der Ausbildung?

## intent:intent_11
- Welche Voraussetzungen benötige ich
- Welche Voraussetzungen benötige ich?

## intent:intent_12
- Was wird im Vorfeld zur Ausbildung empfohlen
- Was wird im Vorfeld zur Ausbildung empfohlen?

## intent:intent_13
- Wo kann ich mich um einen Praktikumsplatz bewerben
- Wo kann ich mich um einen Praktikumsplatz bewerben?

## intent:intent_14
- Wo kann ich mich für das FSJ bewerben
- Wo kann ich mich für das FSJ bewerben?

## intent:intent_15
- Gibt es einen Bewerbungszeitraum
- Gibt es einen Bewerbungszeitraum?

## intent:intent_16
- Wie kann ich mich bewerben
- Wie kann ich mich bewerben?

## intent:intent_17
- An wen muss ich meine Bewerbung richten
- An wen muss ich meine Bewerbung richten?

## intent:intent_18
- Was sollte in meiner Bewerbung enthalten sein
- Was sollte in meiner Bewerbung enthalten sein?

## intent:intent_19
- Wie lange dauert die Bearbeitung meiner Bewerbung
- Wie lange dauert die Bearbeitung meiner Bewerbung?

## intent:intent_20
- Wie verläuft das Bewerbungsverfahren worauf wird Wert gelegt
- Wie verläuft das Bewerbungsverfahren worauf wird Wert gelegt?

## intent:intent_21
- Gibt es bei Vivantes eine Altersgrenze für die Ausbildung
- Gibt es bei Vivantes eine Altersgrenze für die Ausbildung?

## intent:intent_22
- Ich habe meinen Schulabschluss nicht in Deutschland erworben! - Kann ich mich trotzdem bewerben
- Ich habe meinen Schulabschluss nicht in Deutschland erworben! - Kann ich mich trotzdem bewerben?

## intent:intent_23
- Welches Sprachlevel benötige ich für eine Ausbildung
- Welches Sprachlevel benötige ich für eine Ausbildung?

## intent:intent_24
- Warum ein Praktikum vor der Ausbildung
- Warum ein Praktikum vor der Ausbildung?

## intent:intent_25
- Wo kann ich ein Praktikum absolvieren und wie lange sollte das Praktikum sein
- Wo kann ich ein Praktikum absolvieren und wie lange sollte das Praktikum sein?

## intent:intent_26
- Wo kann ich ein Praktikum bei Vivantes absolvieren und wo bewerbe ich mich
- Wo kann ich ein Praktikum bei Vivantes absolvieren und wo bewerbe ich mich?

## intent:intent_27
- Wann sollte ich mich um ein Praktikum bewerben
- Wann sollte ich mich um ein Praktikum bewerben?

## intent:intent_28
- Was gehört in meine Praktikumsbewerbung
- Was gehört in meine Praktikumsbewerbung?

## intent:intent_29
- Praktikum und im Bewerbungsverfahren für eine Ausbildung bei Vivantes
- Praktikum und im Bewerbungsverfahren für eine Ausbildung bei Vivantes?

## intent:intent_30
- Wo findet die praktische Ausbildung statt
- Wo findet die praktische Ausbildung statt?

## intent:intent_31
- Habe ich vor Ort einen Ansprechpartner
- Habe ich vor Ort einen Ansprechpartner?

## intent:intent_32
- Welche Projekte gibt es im Rahmen der praktischen Ausbildung
- Welche Projekte gibt es im Rahmen der praktischen Ausbildung?

## intent:intent_33
- Kann ich einen Einsatz im Ausland machen
- Kann ich einen Einsatz im Ausland machen?

## intent:intent_34
- Wo findet die theoretische Ausbildung statt
- Wo findet die theoretische Ausbildung statt?

## intent:intent_35
- Was beinhaltete die theoretische Ausbildung
- Was beinhaltete die theoretische Ausbildung?

## intent:intent_36
- Wie wird der Unterrichtsstoff vermittelt
- Wie wird der Unterrichtsstoff vermittelt?

## intent:intent_37
- Von wem werde ich unterrichtet
- Von wem werde ich unterrichtet

## intent:intent_38
- Wie werde ich während der Ausbildung unterstützt
- Wie werde ich während der Ausbildung unterstützt?

## intent:intent_39
- Wie viele Auszubildende gibt es am Institut
- Wie viele Auszubildende gibt es am Institut?

## intent:intent_40
- Wie groß sind die Kurse
- Wie groß sind die Kurse?

## intent:intent_41
- Wie kann ich die Pausen verbringen
- Wie kann ich die Pausen verbringen?

## intent:intent_42
- Gibt es vor Ort eine Unterbringungsmöglichkeit
- Gibt es vor Ort eine Unterbringungsmöglichkeit?
